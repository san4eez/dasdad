package com.example.watch;

import androidx.appcompat.app.AppCompatActivity;

import android.app.TaskStackBuilder;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.airbnb.lottie.LottieAnimationView;

public class OrderActivity extends AppCompatActivity {
    LottieAnimationView lottieAnimationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                TaskStackBuilder.create(OrderActivity.this)
                        .addNextIntentWithParentStack(new Intent(OrderActivity.this, MainActivity.class))
                        .addNextIntent(new Intent(OrderActivity.this, MainActivity.class))
                        .startActivities();

            }
        }, 8000);
    }

}