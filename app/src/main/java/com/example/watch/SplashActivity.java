package com.example.watch;

import androidx.appcompat.app.AppCompatActivity;

import android.app.TaskStackBuilder;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.airbnb.lottie.LottieAnimationView;

public class SplashActivity extends AppCompatActivity {
    LottieAnimationView lottieAnimationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                TaskStackBuilder.create(SplashActivity.this)
                        .addNextIntentWithParentStack(new Intent(SplashActivity.this, LoginActivity.class))
                        .addNextIntent(new Intent(SplashActivity.this, LoginActivity.class))
                        .startActivities();

            }
        }, 3000);
    }

}